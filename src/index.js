import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './App'
import countNumber from './store/reducers'

import { Provider } from 'react-redux'
import { createStore } from 'redux'

let store = createStore(countNumber)

// console.log('STORE: ' + JSON.stringify(store.getState()))

ReactDOM.render(
  <Provider store = { store }>
    <App />
  </Provider>,
  document.getElementById('root')
)
