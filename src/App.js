import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import logo from './logo.svg'
import './App.css'

import * as CounterActions from './store/actions'

class App extends PureComponent {
  // figure out how to use this with mapDispatchToProps
  // IncreaseHandler () {
  //   this.props.Increase()
  // }

  render () {
    var number = 0
    const { count } = this.props

    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React, bitch.</h1>
        </header>
        <p>Current number: { count }</p>
        <button onClick = { (number) => {
          // console.log('PROPS: ' + count)
          // this.IncreaseHandler.bind(this) // figure out how to use this with mapDispatchToProps
          this.props.dispatch(CounterActions.minusNumber())
        } }>Subtract</button>
        <button onClick = { (number) => {
          // console.log('PROPS: ' + count)
          // this.IncreaseHandler.bind(this) // figure out how to use this with mapDispatchToProps
          this.props.dispatch(CounterActions.addNumber())
        } }>Add</button>
      </div>
    )
  }
}

function mapStateToProps (state) {
  return {
    count: state.count
  }
}

// needed at the Container level. this.props.dispatch is available if
// this method is not provided
// function mapDispatchToProps (dispatch) {
//   return bindActionCreators(CounterActions, dispatch)
//   // Increase: () => { return dispatch({type: 'ADD_NUMBER'}) }
// }

// export default App
export default connect(
  mapStateToProps
)(App)
