/*
 * action types
 */
export const ADD_NUMBER = 'ADD_NUMBER'
export const MINUS_NUMBER = 'MINUS_NUMBER'

/*
 * action creators
 */
export function addNumber () {
  return { type: ADD_NUMBER }
}

export function minusNumber () {
  return { type: MINUS_NUMBER }
}
