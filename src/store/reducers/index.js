import {
  ADD_NUMBER,
  MINUS_NUMBER
} from '../actions'

const INITIAL_STATE = {
  count: 0
}

export default function countNumber (state = INITIAL_STATE, action) {
  // console.log('REDUCER CALLED -- ' + JSON.stringify(state) + ', ' + action.type)
  switch (action.type) {
    case ADD_NUMBER:
      return {
        ...state,
        count: state.count + 1
      }
    case MINUS_NUMBER:
      return {
        ...state,
        count: state.count - 1
      }
    default:
      return {...state}
  }
}

// export default combineReducers({
//   countNumber
// })
